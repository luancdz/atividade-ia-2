﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATIVIDADE_IA_1._1
{
    class Arvore
    {
        List<Noh> listaNegra = new List<Noh>();
        Stack<int> pilhaFinal = new Stack<int>();

        public Arvore()
        {
            pilhaFinal.Push(5);
            pilhaFinal.Push(4);
            pilhaFinal.Push(3);
            pilhaFinal.Push(2);
            pilhaFinal.Push(1);
        }

        public bool sucesso(Noh no)
        {
            if (no.pilha3.SequenceEqual(pilhaFinal))
            {
                return true;
            }

            else
            {
                return false;
            }           

        }

        public bool montaArvore(Noh no)
        {
            if (no.pilha1.Count > 0)
            {
                if (no.pilha2.Count.Equals(0) || (no.pilha1.Peek() < no.pilha2.Peek()))
                {
                    no.pilha2.Push(no.pilha1.Pop());

                    if (percorreListaNegra(no))
                    {
                        listaNegra.Add(no);
                        return false;
                    }
                    return true;
                }
                else if (no.pilha3.Count.Equals(0) || (no.pilha1.Peek() < no.pilha3.Peek()))
                {
                    no.pilha3.Push(no.pilha1.Pop());

                    if (percorreListaNegra(no))
                    {
                        listaNegra.Add(no);
                        return false;
                    }
                    return true;
                }
            }
            else if (no.pilha2.Count > 0)
            {
                if (no.pilha1.Count.Equals(0) || no.pilha2.Peek() < no.pilha1.Peek())
                {
                    no.pilha1.Push(no.pilha2.Pop());

                    if (percorreListaNegra(no))
                    {
                        listaNegra.Add(no);
                        return false;
                    }
                    return true;
                }
                else if (no.pilha3.Count.Equals(0) || no.pilha2.Peek() < no.pilha3.Peek())
                {
                    no.pilha3.Push(no.pilha2.Pop());

                    if (percorreListaNegra(no))
                    {
                        listaNegra.Add(no);
                        return false;
                    }
                    return true;
                }

            }
            else if (no.pilha3.Count > 0)
            {
                if (no.pilha1.Count.Equals(0) || no.pilha3.Peek() < no.pilha1.Peek())
                {
                    no.pilha1.Push(no.pilha3.Pop());

                    if (percorreListaNegra(no))
                    {
                        listaNegra.Add(no);
                        return false;
                    }
                    return true;
                }
                else if (no.pilha2.Count.Equals(0) || no.pilha3.Peek() < no.pilha2.Peek())
                {
                    no.pilha2.Push(no.pilha3.Pop());

                    if (percorreListaNegra(no))
                    {
                        listaNegra.Add(no);
                        return false;

                    }
                    return true;
                }               
            }
            else
            {
                return false;
            }    
        }

        public bool percorreListaNegra(Noh no)
        {
            foreach (Noh noh in listaNegra)
            {
                if(comparaNoh(no, noh)){
                    return true;
                }
            }
            return false;   
        }

        public bool comparaNoh(Noh no1,Noh noh2)
        {
            if (no1.pilha1.SequenceEqual(noh2.pilha1) && no1.pilha2.SequenceEqual(noh2.pilha2) && no1.pilha3.SequenceEqual(noh2.pilha3))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
