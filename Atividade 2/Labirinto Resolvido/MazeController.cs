﻿class MazeController
    {
        
        static Stack<PosicaoPilha> montaPilha = new Stack<PosicaoPilha>();

        char[,] matriz;
        int linhaAtual;
        int colunaAtual;
        int passos = 0;


        public void setMatriz(char[,] matriz, int linhaInicial, int colunaInicial)
        {
            this.matriz = matriz;
            PosicaoPilha posicaoE = new PosicaoPilha();
            //Coloca na pilha a posicao atual
            posicaoE.setColuna(colunaInicial);
            posicaoE.setLinha(linhaInicial);
            montaPilha.Push(posicaoE);
        }

        public void busca(int i, int j)
        {
            if(matriz[i, j + 1] == 'S')
            {
                Console.WriteLine("BUSCA EFETUADA, COM O TOTAL DE " + passos + " PASSOS");
                System.Console.ReadKey();
            }
            matriz[i, j] = 'E';
            this.passos = passos + 1;

            double cima = Constantes.distancia, baixo = Constantes.distancia, esquerda = Constantes.distancia, direita = Constantes.distancia, menor = Constantes.distancia;

            if (this.matriz[i + 1, j].Equals('0')){
                baixo = calculaPonto(i + 1, j);
            }
            if(this.matriz[i, j + 1].Equals('0'))
            {
                direita = calculaPonto(i, j + 1);
            }
            if(this.matriz[i - 1, j].Equals('0'))
            {
                cima = calculaPonto(i - 1, j);
            }
            if(this.matriz[i, j - 1].Equals('0'))
            {
                esquerda = calculaPonto(i, j - 1);
            }


            List<double> listaPontos = new List<double>();

            listaPontos.Add(baixo);
            listaPontos.Add(direita);
            listaPontos.Add(cima);
            listaPontos.Add(esquerda);

            menor = listaPontos.Min();
            

            //baixo
            if (this.matriz[i + 1, j].Equals('0') && menor == baixo) {
            
                this.matriz[i, j] = '1';
                this.matriz[i + 1, j] = 'E';
                this.linhaAtual = i + 1;
                this.colunaAtual = j;
                PosicaoPilha posicaoE = new PosicaoPilha();
                //Coloca na pilha a posicao atual
                posicaoE.setLinha(i + 1);
                posicaoE.setColuna(j);
                //montaPilha.Push(posicaoE);
                exibe();
                busca(i + 1, j);
            }
                //direita
                else if (this.matriz[i, j + 1].Equals('0') && menor == direita)
                {
                    this.matriz[i, j] = '1';
                    this.matriz[i, j + 1] = 'E';
                    this.linhaAtual = i;
                    this.colunaAtual = j + 1;
                    PosicaoPilha posicaoE = new PosicaoPilha();
                    //Coloca na pilha a posicao atual
                    posicaoE.setLinha(i);
                    posicaoE.setColuna(j + 1);
                    montaPilha.Push(posicaoE);
                    exibe();
                    busca(i, j + 1);


                }

            //cima
            else if (this.matriz[i - 1, j].Equals('0') && cima == menor)
                    {
                        this.matriz[i, j] = '1';
                        this.matriz[i - 1, j] = 'E';
                        this.linhaAtual = i - 1;
                        this.colunaAtual = j;
                        PosicaoPilha posicaoE = new PosicaoPilha();
                        //Coloca na pilha a posicao atual
                        posicaoE.setLinha(i - 1);
                        posicaoE.setColuna(j);
                       // montaPilha.Push(posicaoE);
                        exibe();
                        busca(i - 1, j);
                    }

                        else if (this.matriz[i, j - 1].Equals('0') && menor == esquerda)
                        {
                            this.matriz[i, j] = '1';
                            this.matriz[i, j - 1] = 'E';
                            this.linhaAtual = i;
                            this.colunaAtual = j - 1;
                            PosicaoPilha posicaoE = new PosicaoPilha();
                            //Coloca na pilha a posicao atual
                            posicaoE.setLinha(i);
                            posicaoE.setColuna(j - 1);
                            montaPilha.Push(posicaoE);
                            exibe();
                            busca(i, j - 1);
                        }

                        /*else
                            {
                                this.matriz[i, j] = '1';
                                PosicaoPilha posicaoAnterior = new PosicaoPilha();
                                posicaoAnterior = montaPilha.Peek();
                                montaPilha.Pop();
                                exibe();
                                busca(posicaoAnterior.getLinha(), posicaoAnterior.getColuna());
                            }*/

        } 
        public void exibe(char[,] matriz, int linha, int coluna)
        {
            for(int i = 0; i<linha; i++)
            {
                for(int j= 0; j<coluna; j++)
                {
                    
                    
                    
                        Console.Write(matriz[i, j]);
                 }
                Console.WriteLine();
            }
            System.Console.ReadKey();
        }

        public double calculaPonto(double Ax, double Ay)
        {
            double potencia = 2;
            double X = Ax - 19;
            double Y = Ay - 30;

            return Math.Sqrt(Math.Pow(X, potencia) + Math.Pow(X, potencia));
        }

       

        public void exibe()
        {
            for (int i = 0; i < 21; i++)
            {
                for (int j = 0; j < 31; j++)
                {
                    Console.Write(this.matriz[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            Console.Write(this.passos);
            System.Threading.Thread.Sleep(500);
            Console.Clear();
        }

    }
    