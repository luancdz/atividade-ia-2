﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// Custo : numero de passos h(x) : Distancia euclidiana
namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //System.IO.StreamReader txt = new System.IO.StreamReader(@"D:\workspace_visual\maze01.txt");
            //string text = System.IO.File.ReadAllText(@"D:\workspace_visual\maze01.txt");

            string[] texto;
            string caminho = @"D:\workspace_visual\maze01.txt";

            MazeController mazeController = new MazeController();
            //int linha = mazeController.tamanhoLinha(txt);
            //int coluna = mazeController.tamanhoColuna(txt);


            texto = System.IO.File.ReadAllLines(caminho);
            char[,] matriz = new char[21, 31];

            int i = 0;
            foreach (string line in texto)
            {
                //Console.WriteLine(line);
                line.ToCharArray();
                for (int j = 0; j<31; j++)
                {
                    if (line[j].Equals(' '))
                    {
                        matriz[i, j] = '0'; 
                    }
                    else if(line[j].Equals('E')){
                        matriz[i, j] = 'E';
                    }
                    else if (line[j].Equals('S'))
                    {
                        matriz[i, j] = 'S';
                    }
                    else
                    {
                        matriz[i, j] = '1';
                    }
                    //matriz[i, j] = line[j];
                    //Console.WriteLine(line);
                }
                i++;
                //Console.WriteLine("\t" + line);              

            }

            mazeController.setMatriz(matriz,1,0);
            mazeController.busca(1, 0);
                               
           System.Console.ReadKey();
            
        }
    }

}
